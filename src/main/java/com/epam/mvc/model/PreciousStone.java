package com.epam.mvc.model;

/**
 * Describe precious stones.
 */
public class PreciousStone extends Stone {
    /**
     * save color of each stone.
     */
    private String colour;

    /**
     * @param weight           of one stone.
     * @param transparency     of stone.
     * @param pricePerOneKarat of certain stone.
     * @param color            of stone.
     */
    public PreciousStone(final int weight, final int transparency
            , final int pricePerOneKarat, final String color) {
        super(weight, transparency, pricePerOneKarat);
        this.colour = color;

    }

    /**
     * @return color of stone.
     */
    public final String getColour() {
        return colour;
    }

    /**
     * @param colour sets to variable color.
     */
    public final void setColour(final String colour) {
        this.colour = colour;
    }

    /**
     * @param weight of stone.
     * @return price of stone.
     */
    @Override
    public final int calculatePrice(final int weight) {
        int tempPrice = weight * getPricePerOneKarat() + getTransparency();
        if (colour != null) {
            tempPrice *= 2;
        }
        return tempPrice;
    }

    /**
     * @return string which describes object.
     */
    @Override
    public final String toString() {
        return "PreciousStone{" + super.toString() + "color=" + colour + '}';
    }
}
