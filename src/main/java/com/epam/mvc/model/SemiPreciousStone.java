package com.epam.mvc.model;

/**
 * Describes semiprecious stones.
 */
public class SemiPreciousStone extends Stone {
    /**
     * save shape of stone.
     */
    private int shape;

    /**
     * @param weight           of one stone.
     * @param transparency     of stone.
     * @param pricePerOneKarat of certain stone.
     * @param shape            of stone.
     */
    public SemiPreciousStone(final int weight, final int transparency
            , final int pricePerOneKarat, final int shape) {
        super(weight, transparency, pricePerOneKarat);
        this.shape = shape;
    }

    /**
     * @return shape of stone.
     */
    public final int getShape() {
        return shape;
    }

    /**
     * @param shape sets to variable shape.
     */
    public final void setShape(final int shape) {
        this.shape = shape;
    }

    /**
     * @param weight of stone.
     * @return price of stone.
     */
    @Override
    public final int calculatePrice(final int weight) {
        return weight * getPricePerOneKarat() + getTransparency() + shape;
    }

    /**
     * @return string which describes object.
     */
    @Override
    public final String toString() {
        return "SemiPreciousStone{" + super.toString() + "shape=" + shape + '}';
    }
}
