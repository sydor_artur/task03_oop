package com.epam.mvc.controller;

import com.epam.mvc.model.Stone;

import java.util.List;

/**
 *Link model and view.
 */
public interface Controller {
    /**
     *
     * @return list of Stone objects.
     */
    List<Stone> getSetOfStone();
    /**
     *
     * @return list of Stone objects with transparency
     * between 20-80.
     */
    List<Stone> getRangeSetOfStone();
    /**
     * generate new set of Stone objects.
     */
    void generateSet();
    /**
     * sort Stone objects by price.
     */
    void sortByPrice();
    /**
     *
     * @return price of all stones in the set.
     */
    int getPriceOfSet();
    /**
     *
     * @return general weight of set.
     */
    int getWeightOfSet();
}
