package com.epam.mvc.model;

import java.util.List;

/**
 * Interface Model.
 */
public interface Model {
    /**
     * @return list of Stone object.
     */
    List<Stone> getSetOfStone();
    /**
     * @param begin is the start of range.
     * @param end   is the end of range.
     * @return range of stones with transparency between begin and end.
     */
    List<Stone> getRangeSetOfStone(int begin, int end);
    /**
     * Initialise set with Stone objects.
     */
    void generateNewSet();
    /**
     * Sort Stone objects by price.
     */
    void sortByPrice();
    /**
     * @return price of all stone in the set.
     */
    int getPriceOfSet();
    /**
     * @return general weight of set.
     */
    int getWeightOfSet();
}
