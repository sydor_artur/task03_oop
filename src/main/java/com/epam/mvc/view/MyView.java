package com.epam.mvc.view;

import com.epam.mvc.controller.ControllerImp;
import com.epam.mvc.controller.Controller;
import com.epam.mvc.model.Stone;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Class MyView print information in console.
 */
public class MyView {
    /**
     * controller link model and view.
     */
    private Controller controller;
    /**
     * This variable saves menu  information.
     */
    private Map<String, String> menu;
    /**
     * This variable link menu and user choice.
     */
    private Map<String, Printable> methodMenu;
    /**
     * Take user input.
     */
    private Scanner scanner = new Scanner(System.in);

    /**
     * Initialise menu.
     */
    public MyView() {
        controller = new ControllerImp();
        menu = new LinkedHashMap<>();
        menu.put("1", "1-Print set of stones");
        menu.put("2", "2-Show weight of set");
        menu.put("3", "3-Show price of set");
        menu.put("4", "4-Show sorted set by price");
        menu.put("5", "5-Show range of stones with certain transparency");
        menu.put("Q", "Q-Quite");

        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::pressButton1);
        methodMenu.put("2", this::pressButton2);
        methodMenu.put("3", this::pressButton3);
        methodMenu.put("4", this::pressButton4);
        methodMenu.put("5", this::pressButton5);
    }

    /**
     * This method show all stones in the set.
     */
    private void pressButton1() {
        for (Stone stone : controller.getSetOfStone()) {
            System.out.println(stone.toString());
        }
    }

    /**
     * Show general weight of set.
     */
    private void pressButton2() {
        System.out.print("Weight of set: " + controller.getWeightOfSet());
    }

    /**
     * Show general price of set.
     */
    private void pressButton3() {
        System.out.print("Price of set: " + controller.getPriceOfSet());
    }

    /**
     * Sort set of stones by price and print them.
     */
    private void pressButton4() {
        controller.sortByPrice();
        for (Stone stone : controller.getSetOfStone()) {
            System.out.println(stone.toString());
        }
    }

    /**
     * Show stones with certain transparency.
     */
    private void pressButton5() {
        List<Stone> list = controller.getRangeSetOfStone();
        for (Stone stone : list) {
            System.out.println(stone.toString());
        }
    }

    /**
     * Print menu in console.
     */
    private void printMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * This method control menu.
     */
    public final void show() {
        String key;
        do {
            printMenu();
            System.out.println("Make choice");
            key = scanner.nextLine().toUpperCase();
            try {
                methodMenu.get(key).print();
            } catch (Exception e) {
                System.out.println("Invalid input...");
                e.printStackTrace();
            }
        } while (!key.equals("Q"));
    }

}
