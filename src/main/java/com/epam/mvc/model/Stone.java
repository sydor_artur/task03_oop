package com.epam.mvc.model;

/**
 * Describes abstract stone.
 */
public abstract class Stone implements Comparable<Stone> {
    /**
     * save weight of certain stone.
     */
    private int weight;
    /**
     * save price of certain stone.
     */
    private int priceOfStone;
    /**
     * save transparency of certain stone.
     */
    private int transparency;
    /**
     * save price per one carat of stone.
     */
    private int pricePerOneKarat;

    /**
     * Initialize stone.
     *
     * @param weight           of one stone.
     * @param transparency     of stone.
     * @param pricePerOneKarat of certain stone.
     */
    public Stone(final int weight, final int transparency,
                 final int pricePerOneKarat) {
        this.weight = weight;
        this.transparency = transparency;
        this.pricePerOneKarat = pricePerOneKarat;
        priceOfStone = calculatePrice(this.weight);
    }

    /**
     * @return weight of stone.
     */
    public final int getWeight() {
        return weight;
    }

    /**
     * @return price of stone.
     */
    public final int getPriceOfStone() {
        return priceOfStone;
    }

    /**
     * @return transparency of stone.
     */
    public final int getTransparency() {
        return transparency;
    }

    /**
     * @return price per one karat of stone.
     */
    public final int getPricePerOneKarat() {
        return pricePerOneKarat;
    }

    /**
     * @param weight is assigned to variable weight.
     */
    public final void setWeight(final int weight) {
        this.weight = weight;
    }

    /**
     * @param priceOfStone is assigned to variable priceOfStone.
     */
    public final void setPriceOfStone(final int priceOfStone) {
        this.priceOfStone = priceOfStone;
    }

    /**
     * @param transparency id assigned to variable transparency.
     */
    public final void setTransparency(final int transparency) {
        this.transparency = transparency;
    }

    /**
     * @param pricePerOneKarat is assigned to variable pricePerOneKarat.
     */
    public final void setPricePerOneKarat(final int pricePerOneKarat) {
        this.pricePerOneKarat = pricePerOneKarat;
    }

    /**
     * @param weight help to calculate price.
     * @return price of stone.
     */
    public abstract int calculatePrice(final int weight);

    /**
     * @param stone which is compare to current stone.
     * @return result of comparision by price.
     */
    @Override
    public final int compareTo(final Stone stone) {
        if (this.priceOfStone == stone.priceOfStone) {
            return 0;
        } else if (this.priceOfStone < stone.priceOfStone) {
            return -1;
        } else {
            return 1;
        }

    }

    /**
     * @return string which describes stone.
     */
    @Override
    public String toString() {
        return "weight=" + weight
                + ", priceOfStone=" + priceOfStone
                + ", transparency=" + transparency
                + ", pricePerOneKarat=" + pricePerOneKarat
                + ',';
    }
}
