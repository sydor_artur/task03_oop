package com.epam.mvc.controller;

import com.epam.mvc.model.Domain;
import com.epam.mvc.model.Model;
import com.epam.mvc.model.Stone;

import java.util.List;

/**
 *Implements interface Controller.
 */
public class ControllerImp implements Controller {
    /**
     *Reference variable on model.
     */
    private Model model;

    /**
     * Initialize variable model.
     */
    public ControllerImp() {
        model = new Domain();
    }

    /**
     *
     * @return list of Stone objects.
     */
    @Override
    public final List<Stone> getSetOfStone() {
        return model.getSetOfStone();
    }

    /**
     *
     * @return list of Stone objects with transparency
     * between 20-80.
     */
    @Override
    public final List<Stone> getRangeSetOfStone() {
        return model.getRangeSetOfStone(20, 80);
    }

    /**
     * generate new set of Stone objects.
     */
    @Override
    public final void generateSet() {
        model.generateNewSet();
    }

    /**
     * sort Stone objects by price.
     */
    @Override
    public final void sortByPrice() {
        model.sortByPrice();
    }

    /**
     *
     * @return price of all stones in the set.
     */
    @Override
    public final int getPriceOfSet() {
        return model.getPriceOfSet();
    }

    /**
     *
     * @return general weight of set.
     */
    @Override
    public final int getWeightOfSet() {
        return model.getWeightOfSet();
    }
}
