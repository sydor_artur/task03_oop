package com.epam.mvc;

import com.epam.mvc.view.MyView;

/**
 * This class is the start point of program.
 */
public final class App {
    /**
     * Private constructor for utility class.
     */
    private App() {

    }
    /**
     * Create object MyView and start execution of program.
     * @param args
     * Array of strings.
     */
    public static void main(final String[] args) {
        new MyView().show();
    }
}
