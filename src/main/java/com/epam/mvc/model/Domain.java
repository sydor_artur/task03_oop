package com.epam.mvc.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Contain application data.
 */
public class Domain implements Model {
    /**
     * save precious and semiprecious stones.
     */
    private List<Stone> setOfStone;
    /**
     * save weight of all stones in the set.
     */
    private int weightOfSet;
    /**
     * save price of all stones in the set.
     */
    private int priceOfSet;
    /**
     * hepl to generate random numbers.
     */
    private Random rand;

    /**
     * Initialize Domain object.
     */
    public Domain() {
        setOfStone = new ArrayList<>();
        rand = new Random();
        generateNewSet();
    }

    /**
     * @return list of Stone object.
     */
    @Override
    public final List<Stone> getSetOfStone() {
        if (setOfStone.isEmpty()) {
            generateNewSet();
        }
        return setOfStone;
    }

    /**
     * @param begin is the start of range.
     * @param end   is the end of range.
     * @return range of stones with transparency between begin and end.
     */
    @Override
    public final List<Stone> getRangeSetOfStone(final int begin
            , final int end) {
        List<Stone> stoneOfRange = new ArrayList<>();
        for (Stone stone : setOfStone) {
            if (stone.getTransparency() >= begin
                    && stone.getTransparency() <= end) {
                stoneOfRange.add(stone);
            }
        }
        return stoneOfRange;
    }

    /**
     * Initialise set with Stone objects.
     */
    @Override
    public final void generateNewSet() {
        int numberOfPrecious;
        int numberOfSemiPrecious;

        Random rand = new Random();
        numberOfPrecious = rand.nextInt(10 + 1) + 1;
        for (int i = 1; i <= numberOfPrecious; i++) {
            setOfStone.add(generatePrecious());
        }

        numberOfSemiPrecious = rand.nextInt(10 + 1) + 1;
        for (int i = 1; i <= numberOfSemiPrecious; i++) {
            setOfStone.add(generateSemiPrecious());
        }
    }

    /**
     * Sort Stone objects by price.
     */
    @Override
    public final void sortByPrice() {
        if (!setOfStone.isEmpty()) {
            Collections.sort(setOfStone);
        } else {
            System.out.println("There are no stones...");
        }
    }

    /**
     * @return price of all stone in the set.
     */
    @Override
    public final int getPriceOfSet() {
        for (Stone stone : setOfStone) {
            priceOfSet += stone.getPriceOfStone();
        }
        return priceOfSet;
    }

    /**
     * @return general weight of set.
     */
    @Override
    public final int getWeightOfSet() {
        for (Stone stone : setOfStone) {
            weightOfSet += stone.getWeight();
        }
        return weightOfSet;
    }

    /**
     * @return object of type PreciousStone.
     */
    private PreciousStone generatePrecious() {
        String[] colors = {"Red", "White", "Yellow", "Blue", "Green"};
        int tempWeight = rand.nextInt(10 + 1) + 1;
        int tempTransparency = rand.nextInt(100 + 1) + 1;
        int tempPrice = rand.nextInt(1000 + 1) + 100;
        String tempColor = colors[rand.nextInt(5)];
        return new PreciousStone(tempWeight, tempTransparency,
                tempPrice, tempColor);
    }

    /**
     * @return object of type SemiPreciousStone.
     */
    private SemiPreciousStone generateSemiPrecious() {
        int tempWeight = rand.nextInt(10 + 1) + 1;
        int tempTransparency = rand.nextInt(100 + 1) + 1;
        int tempPrice = rand.nextInt(1000 + 1) + 100;
        int tempShape = rand.nextInt(40 + 1) + 10;
        return new SemiPreciousStone(tempWeight, tempTransparency,
                tempPrice, tempShape);
    }
}
